import React from "react";
import "./Home.css";

const Home = ({ sair }) => (
  <div className="wrapper-home">
    <header>
      <span>Logo</span>
      <span onClick={sair} role="presentation">
        Sair
      </span>
    </header>
    <h2>Home</h2>
  </div>
);

export default Home;
