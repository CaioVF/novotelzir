import React, { useState } from "react";
import Login from "./Login";
import "./App.css";
import Home from "./Home";

const App = () => {
  const [valor, setaValor] = useState(0);

  const next = () => {
    setaValor(100);
  };

  const prev = () => {
    setaValor(0);
  };

  const styleContent = {
    transform: `translate(-${valor}%)`
  };

  return (
    <div className="app">
      <div className="content-login" style={styleContent}>
        <Login submit={next} />
      </div>
      <div className="content-home" style={styleContent}>
        <Home sair={prev} />
      </div>
    </div>
  );
};

export default App;
