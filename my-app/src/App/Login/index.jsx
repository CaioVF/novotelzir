import React, { useState } from "react";
import "./Login.css";

const defaultState = {
  firstName: null,
  lastName: null,
  email: null,
  password: null,
  formErrors: {
    firstName: "",
    lastName: "",
    email: "",
    password: ""
  }
};

const Login = ({ submit }) => {
  const [state, setState] = useState(defaultState);

  const handleSubmit = e => {
    e.preventDefault();
    submit();
  };

  const handleChange = evt => {
    const { name, value } = evt.target;

    setState(prevState => ({
      ...prevState,
      [name]: value
    }));
  };

  return (
    <div className="wrapper">
      <div className="form-wrapper">
        <h1>Create Account</h1>
        <form onSubmit={handleSubmit} noValidate>
          <div className="firstName">
            <label htmlFor="firstName">First Name</label>
            <input
              type="text"
              className=""
              placeholder="First Name"
              name="firstName"
              noValidate
              onChange={handleChange}
            />
          </div>
          <div className="lastName">
            <label htmlFor="lastName">Last Name</label>
            <input
              type="text"
              className=""
              placeholder="Last Name"
              name="lastName"
              noValidate
              onChange={handleChange}
            />
          </div>
          <div className="email">
            <label htmlFor="email">Email</label>
            <input
              type="text"
              className=""
              placeholder="Email"
              name="email"
              type="email"
              noValidate
              onChange={handleChange}
            />
          </div>
          <div className="password">
            <label htmlFor="password">Password</label>
            <input
              type="text"
              className=""
              placeholder="Password"
              name="password"
              type="password"
              noValidate
              onChange={handleChange}
            />
          </div>
          <div className="createAccount">
            <button type="submit">Create Account</button>
            <small>Already Have a Account?</small>
          </div>
        </form>
      </div>
    </div>
  );
};

// class Login extends Component {
//   constructor(props) {
//     super(props);

//     this.state = {
//       firstName: null,
//       lastName: null,
//       email: null,
//       password: null,
//       formErrors: {
//         firstName: "",
//         lastName: "",
//         email: "",
//         password: ""
//       }
//     };
//   }

//   handleSubmit = e => {
//     // const { history } = this.props;

//     e.preventDefault();
//     // history.push('/home');
//   };

//   render() {
//     return (
//       <div className="wrapper">
//         <div className="form-wrapper">
//           <h1>Create Account</h1>
//           <form onSubmit={this.handleSubmit} noValidate>
//             <div className="firstName">
//               <label htmlFor="firstName">First Name</label>
//               <input
//                 type="text"
//                 className=""
//                 placeholder="First Name"
//                 name="firstName"
//                 noValidate
//                 onChange={this.handleChange}
//               />
//             </div>
//             <div className="lastName">
//               <label htmlFor="lastName">Last Name</label>
//               <input
//                 type="text"
//                 className=""
//                 placeholder="lastName"
//                 name="lastName"
//                 noValidate
//                 onChange={this.handleChange}
//               />
//             </div>
//             <div className="email">
//               <label htmlFor="email">Email</label>
//               <input
//                 type="text"
//                 className=""
//                 placeholder="Email"
//                 name="email"
//                 type="email"
//                 noValidate
//                 onChange={this.handleChange}
//               />
//             </div>
//             <div className="password">
//               <label htmlFor="password">Password</label>
//               <input
//                 type="text"
//                 className=""
//                 placeholder="Password"
//                 name="password"
//                 type="password"
//                 noValidate
//                 onChange={this.handleChange}
//               />
//             </div>
//             <div className="createAccount">
//               <button type="submit">Create Account</button>
//               <small>Already Have a Account?</small>
//             </div>
//           </form>
//         </div>
//       </div>
//     );
//   }
// }

export default Login;
